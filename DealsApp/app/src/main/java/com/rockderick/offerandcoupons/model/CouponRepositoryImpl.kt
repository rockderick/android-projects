package com.rockderick.offerandcoupons.model

import android.util.Log
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.rockderick.offerandcoupons.R
import com.rockderick.offerandcoupons.presenter.CouponPresenter
import com.rockderick.offerandcoupons.view.RecyclerCouponsAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CouponRepositoryImpl(var couponPresenter: CouponPresenter): CouponRepository {
    override fun getCouponsAPI() {

        //CONTROLLER
        var coupons: ArrayList<Coupon> = ArrayList()
        val apiAdapter : ApiAdapter = ApiAdapter()
        val apiService = apiAdapter.getClientService()
        val call = apiService.getCoupons()

        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                Log.e("ERROR: ", t.message)
                t.stackTrace
            }

            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                val offersJsonArray = response.body()?.getAsJsonArray("offers")
                offersJsonArray?.forEach { jsonElement: JsonElement ->
                    var jsonObject = jsonElement.asJsonObject
                    var coupon = Coupon(jsonObject)
                    coupons.add(coupon)
                }

                couponPresenter.showCoupons(coupons)

                //CONTROLLER



            }
        })


        //couponPresenter.showCoupons()
    }
}