package com.rockderick.offerandcoupons.model

import com.rockderick.offerandcoupons.presenter.CouponPresenter

class CouponsInteractorImpl(var couponPresenter: CouponPresenter) : CouponsInteractor {

    private var couponRepository :CouponRepository = CouponRepositoryImpl(couponPresenter)

    override fun getCouponsAPI() {
        couponRepository.getCouponsAPI()
    }
}