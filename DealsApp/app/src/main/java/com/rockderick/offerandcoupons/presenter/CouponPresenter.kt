package com.rockderick.offerandcoupons.presenter

import com.rockderick.offerandcoupons.model.Coupon

interface CouponPresenter {
    //Interactor
    fun getCoupons()
    //Vista
    fun showCoupons(coupons: ArrayList<Coupon>)
}