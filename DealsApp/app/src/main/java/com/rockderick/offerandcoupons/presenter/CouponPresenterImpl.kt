package com.rockderick.offerandcoupons.presenter

import com.rockderick.offerandcoupons.model.Coupon
import com.rockderick.offerandcoupons.model.CouponsInteractor
import com.rockderick.offerandcoupons.model.CouponsInteractorImpl
import com.rockderick.offerandcoupons.view.CouponView

class CouponPresenterImpl(var couponView: CouponView) : CouponPresenter{

    private var couponInteractor: CouponsInteractor = CouponsInteractorImpl(this)

    override fun getCoupons() {

        couponInteractor.getCouponsAPI()
    }

    override fun showCoupons(coupons: ArrayList<Coupon>) {
        couponView.showCoupons(coupons)
    }
}