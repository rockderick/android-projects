package com.rockderick.offerandcoupons.view

import com.rockderick.offerandcoupons.model.Coupon

interface CouponView {
    //Presentador
    fun getCoupons()
    //Vista
    fun showCoupons(coupons: ArrayList<Coupon>)
}