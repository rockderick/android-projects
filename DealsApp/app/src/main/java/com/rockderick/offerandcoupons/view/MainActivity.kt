package com.rockderick.offerandcoupons.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.rockderick.offerandcoupons.model.Coupon
import com.rockderick.offerandcoupons.R
import com.rockderick.offerandcoupons.model.ApiAdapter
import com.rockderick.offerandcoupons.presenter.CouponPresenter
import com.rockderick.offerandcoupons.presenter.CouponPresenterImpl
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity(),CouponView {

    private var couponPresenter: CouponPresenter? = null
    lateinit var rvCoupons: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()

        couponPresenter = CouponPresenterImpl(this)

        //VIEW
        rvCoupons= findViewById(R.id.rvCoupons)
        rvCoupons.layoutManager = LinearLayoutManager(this)
        getCoupons()
        //val coupons = ArrayList<Coupon>()
        //VIEW


                
                //VIEW
                //rvCoupons.adapter = RecyclerCouponsAdapter(coupons, R.layout.card_coupon)
                //VIEW




    }

    override fun getCoupons() {
        couponPresenter?.getCoupons()

    }

    override fun showCoupons(coupons: ArrayList<Coupon>) {
        //VIEW
        rvCoupons.adapter = RecyclerCouponsAdapter(coupons, R.layout.card_coupon)
        //VIEW

    }
}