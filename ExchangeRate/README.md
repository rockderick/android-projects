ExchangeRate Application
=========================================

Introduction
------------

This is an application that use ExchangeRates API(https://exchangeratesapi.io/) in order to visualize the exchange rate
between dollar and euro. 


What the app does
--------------------

1. Visualize the actual current exchange rate between dollar and Euro.
2. Display the exchange rate between dollar and euro from the last 7 days.



Implementation Notes
--------------------
1. Architecture: MVVM
2. Kotlin coroutines
3. Retrofit and Moshi


General Notes
--------------------
This repository is organized so that you can visualize my coding progress by
visiting the following branches:

1. 1_Networking: Branch that fetch the data of the fixer API(https://gitlab.com/rockderick/ExchangeRate/-/tree/1_Networking)
2. 2_UserInterface: Branch that build the User Interface(https://gitlab.com/rockderick/ExchangeRate/-/tree/2_UserInterface)
3. 3_3_Connect_Networkig_w_UI: Branch that connect UI with the networking layer(https://gitlab.com/rockderick/ExchangeRate/-/tree/3_Connect_Networkig_w_UI)






Screenshots
-----------

![](screenshoots/screenshoot.png)




