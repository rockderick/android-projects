package com.rockderick.exchangerate.model

data class HistoricalRate(val base: String, val date: String,val rates:Rate)

data class Rate(val USD:String)
