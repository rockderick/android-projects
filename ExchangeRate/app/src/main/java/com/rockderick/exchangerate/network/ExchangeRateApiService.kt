package com.rockderick.exchangerate.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.rockderick.exchangerate.model.HistoricalRate
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

private const val BASE_URL = "http://api.exchangeratesapi.io/v1/"


private val moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()


private val retrofit = Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .baseUrl(BASE_URL)
        .build()



interface ExchangeRateApiService {
    @GET("{date}")
    fun getProperties(@Path("date") date: String,
                      @Query("access_key") apiKey: String?,
                      @Query("symbols") language: String="USD"
                      ): Deferred<HistoricalRate>
}


object ExchangeRateApi {
    val RETROFIT_SERVICE_EXCHANGE_RATE : ExchangeRateApiService by lazy {
        retrofit.create(ExchangeRateApiService::class.java)
    }
}
