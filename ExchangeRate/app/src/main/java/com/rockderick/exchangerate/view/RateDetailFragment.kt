package com.rockderick.exchangerate.view


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.anychart.AnyChart
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.chart.common.dataentry.ValueDataEntry
import com.anychart.data.Mapping
import com.anychart.data.Set
import com.anychart.enums.Anchor
import com.anychart.enums.HoverMode
import com.anychart.enums.Position
import com.anychart.enums.TooltipPositionMode
import com.rockderick.exchangerate.R
import com.rockderick.exchangerate.databinding.FragmentRateDetailBinding
import com.rockderick.exchangerate.viewmodel.RateDetailViewModel


class RateDetailFragment : Fragment() {

    private val viewModel: RateDetailViewModel by lazy {
        ViewModelProvider(this).get(RateDetailViewModel::class.java)
    }

    private lateinit var binding: ViewDataBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentRateDetailBinding.inflate(inflater)




        // Allows Data Binding to Observe LiveData with the lifecycle of this Fragment
        binding.lifecycleOwner = this

        // Giving the binding access to the OverviewViewModel
        binding.viewModel = viewModel



        viewModel.property.observe(this.viewLifecycleOwner, Observer {
            val usdVal = it.rates.USD.toDouble()
            binding.usdRateTv.text = "%.2f".format(usdVal)
            binding.usdValueTv.text = ("%.2f".format(1 / usdVal))
            binding.textConversionTv.text = ("%.2f".format(100 * usdVal))
            binding.progressBar1.setVisibility(View.GONE);


        })

        viewModel.historicalRateObservableList.observe(this.viewLifecycleOwner, Observer {
            binding.anyChartView.setProgressBar(binding.progressBar2)

            val area3d = AnyChart.area3d()

            area3d.xAxis(0).labels().format("{%Value}")

            area3d.animation(true)

            area3d.yAxis(0).title("Dollar Value")
            area3d.xAxis(0).title("Date")
            area3d.xAxis(0).labels().padding(5.0, 5.0, 0.0, 5.0)

            area3d.title(
                """Euro Dollar Rate<br/>' +
           '<span style="color:#212121; font-size: 13px;">Statics from the last 7 days</span>"""
            )

            area3d.title().useHtml(true)
            area3d.title().padding(0.0, 0.0, 20.0, 0.0)


            class CustomDataEntry(x: String?, value: Number?) :
                ValueDataEntry(x, value) { }

            val seriesData: MutableList<DataEntry> = ArrayList()

            for (element in it) {
                seriesData.add(CustomDataEntry(element.date, element.rates.USD.toDouble()))
            }

            val set: Set = Set.instantiate()
            set.data(seriesData)
            val series1Data: Mapping = set.mapAs("{ x: 'x', value: 'value' }")


            val series1 = area3d.area(series1Data)
            series1.name("Value")
            series1.hovered().markers(false)
            series1.hatchFill("diagonal", "#000", 0.6, 10.0)



            area3d.tooltip()
                .position(Position.CENTER_TOP)
                .positionMode(TooltipPositionMode.POINT)
                .anchor(Anchor.LEFT_BOTTOM)
                .offsetX(5.0)
                .offsetY(5.0)

            area3d.interactivity().hoverMode(HoverMode.BY_X)
            area3d.zAspect("100%")


            binding.anyChartView.setChart(area3d)


        })

        return binding.root



    }





}