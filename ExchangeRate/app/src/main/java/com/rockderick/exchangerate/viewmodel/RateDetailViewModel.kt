package com.rockderick.exchangerate.viewmodel

import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rockderick.exchangerate.model.HistoricalRate
import com.rockderick.exchangerate.network.ExchangeRateApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.joda.time.DateTime
import org.joda.time.LocalDate
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter


class RateDetailViewModel: ViewModel() {

   private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)


    // The internal MutableLiveData String that stores the status of the most recent request
    private val _status = MutableLiveData<String>()
    // The external immutable LiveData for the request status String
    val response: LiveData<String>
        get() = _status

    //The internal MutableLiveData HistoricalRequest that stores the status of the most recent request
    private val _property = MutableLiveData<HistoricalRate>()
    // The external immutable LiveData for the request the HistoricalRate from the request
    val property: LiveData<HistoricalRate>
        get() = _property

     val historicalRateList: MutableList<HistoricalRate> = ArrayList<HistoricalRate>()


    //LiveData to store a List of HistoricalRate so that we can graph the Data
    private val _historicalRateObservableList = MutableLiveData<MutableList<HistoricalRate>>()

    val historicalRateObservableList: LiveData<MutableList<HistoricalRate>>
        get() = _historicalRateObservableList


    /**
     * Call getDollarEuroRateEstateProperties() and  getDollarEuroRateTableData on init so we can display status immediately.
     */
    init {
        val now = DateTime()
        val today: LocalDate = now.toLocalDate()
        val dtfOut: DateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd")
        getDollarEuroRateEstateProperties(today.toString(dtfOut))

        getDollarEuroRateTableData(7)


    }

    /**
     * Sets the value of the status LiveData to ExchangeRate API status.
     */
    private fun getDollarEuroRateEstateProperties(date: String) {

        coroutineScope.launch {
            var getPropertiesDeferred = ExchangeRateApi.RETROFIT_SERVICE_EXCHANGE_RATE.getProperties(
                date,
                "2e66d74fecbd282307d48ca7163283b3"
            )

            try{
                var result = getPropertiesDeferred.await()
                _status.value = "Success: ${result} API info retrieved"
                _property.value = result
            }catch (t: Throwable){
                _status.value = "Failure: " + t.message
            }
        }


    }

    private fun getDollarEuroRateTableData(days: Int){

        coroutineScope.launch {

            val now = DateTime()
            val today: LocalDate = now.toLocalDate()
            val dtfOut: DateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd")
            for(i in days downTo 0) {

            var dayToFetch = today.minusDays(i).toString(dtfOut)
                var getPropertiesDeferred =
                    ExchangeRateApi.RETROFIT_SERVICE_EXCHANGE_RATE.getProperties(
                        dayToFetch,
                        "2e66d74fecbd282307d48ca7163283b3"
                    )
                try {
                    var result = getPropertiesDeferred.await()
                    _status.value = "Success: ${result} API info retrieved"

                    historicalRateList.add(result)

                } catch (e: Exception) {
                    _status.value = "Failure: ${e.printStackTrace()}"
                    Log.v("Failure", _status.value.toString())
                }


            }

            _historicalRateObservableList.value=historicalRateList
        }

    }
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}






