MovieBox Application
=========================================

Introduction
------------

This is an application that use The Movie Database (TMDB) API(https://developers.themoviedb.org/3/getting-started/introduction) in order to visualize data from recent movies. 


What the app does
--------------------

1. List horizontally currently playing movies
2. Display the most popular movies in the vertical list view, with multiple pages
3. When a user clicks on any movie list item, it will navigate to a detailed screen, with more information about the movie



Implementation Notes
--------------------
1. Architecture: MVVM
2. Kotlin coroutines
3. Flow
4. Retrofit
5. Navigation Component

Screenshots
-----------

![](screenshoots/screenshot1.png)
![](screenshoots/screenshot2.png)













