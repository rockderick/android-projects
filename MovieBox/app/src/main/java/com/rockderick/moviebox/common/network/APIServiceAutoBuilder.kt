package com.rockderick.moviebox.common.network


import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import okhttp3.Interceptor
import okhttp3.internal.toImmutableMap
import java.lang.Exception
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException

abstract class APIServiceAutoBuilder<T : Any>(
    private val headers: Map<String, String>? = null,
    baseUrl: String = BaseUrl.GetBaseUrl().base_url,
    isRefreshTokenRequired :Boolean= true
) {
    var client = getRetrofitApi(baseUrl, getInterceptor(baseUrl, isRefreshTokenRequired))

    private fun getRetrofitApi(
        baseUrl: String,
        okBuilder: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(okBuilder)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun getInterceptor(baseUrl: String, isRefreshTokenRequired :Boolean): OkHttpClient {
        val interceptor = Interceptor { chain ->
            val request = chain.request().newBuilder()

            request.addHeader("Content-Type", "application/json")

            val apiIdentifiersHeaders = (headers ?: getNoSessionApiIdentifiersHeaders()).toMutableMap()
            apiIdentifiersHeaders[headerKey_ApiKey] = ""

            var requestEnd = request.build()
            val response = chain.proceed(requestEnd)
            response
        }

        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        return OkHttpClient().newBuilder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor(interceptor)
            .connectTimeout(60, TimeUnit.SECONDS)
            .callTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .build()
    }

    fun getNoSessionApiIdentifiersHeaders(): Map<String, String> {
        val headers = mutableMapOf<String, String>()
        headers[headerKey_ApiKey] = ""
        return headers.toImmutableMap()
    }

    abstract fun build(): T

    companion object {
        const val headerKey_Authorization = "Authorization"
        const val headerKey_ApiKey = "x-api-key"
        const val headerKey_UserId = "X-User-Id"
        const val headerKey_IdentityId = "X-Identitiy-Id"
        const val headerKey_OperatorId = "X-Operator-Id"


        suspend inline fun <T> executeEndPoint(
            crossinline callFunction: suspend () -> T
        ): Flow<ApiServiceState<T>>? {
            return flow {
                emit(ApiServiceState.Loading)
                try {
                    val data = callFunction.invoke()
                    emit(ApiServiceState.Success(data))
                } catch (exception: Exception) {
                    exception.printStackTrace()

                    when (exception) {
                        is HttpException -> emit(ApiServiceState.Error(ErrorNetwork().withCode(exception.code()).withMessage(exception.message!!)))
                        is IOException -> emit(ApiServiceState.Error(ErrorNetwork().withCode(4000).withMessage(exception.message!!)))
                        is SocketTimeoutException -> emit(ApiServiceState.Error(ErrorNetwork().withCode(4001).withMessage(exception.message!!)))
                        else -> emit(ApiServiceState.Error(ErrorNetwork().withCode(4002).withMessage(exception.message!!)))
                    }
                }
            }
        }
    }
}