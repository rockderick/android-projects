package com.rockderick.moviebox.common.network

sealed class ApiServiceState <out R> {
     data class Success<out T>(val data: T): ApiServiceState<T>()
     data class Error <out ErrorNetwork> (val exception: ErrorNetwork): ApiServiceState<Nothing>()
     object Loading: ApiServiceState<Nothing>()
}