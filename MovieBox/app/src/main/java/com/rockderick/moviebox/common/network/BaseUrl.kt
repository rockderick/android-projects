package com.rockderick.moviebox.common.network

sealed class BaseUrl {
    class GetBaseUrl: BaseUrl() {
        val base_url   : String = "https://api.themoviedb.org/"
        val api_key:String = "6d295295d8fd3f6ffcdbf919f4e9ea03"
    }
}