package com.rockderick.moviebox.common.network

class ErrorNetwork {
    var code: Int=0
    var message: String=""

    fun withCode(mCode: Int): ErrorNetwork = this.apply { code = mCode }
    fun withMessage(mMessage: String): ErrorNetwork = this.apply { message = mMessage }

}