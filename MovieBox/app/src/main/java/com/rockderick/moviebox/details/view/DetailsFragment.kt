package com.rockderick.moviebox.details.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.rockderick.moviebox.R
import com.rockderick.moviebox.databinding.FragmentDetailsBinding
import com.rockderick.moviebox.databinding.FragmentHomeBinding


class DetailsFragment : Fragment() {

    private lateinit var binding: FragmentDetailsBinding
    private val args  by navArgs<DetailsFragmentArgs>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_details,container,false)

        args.movie.let {
            binding.tvTitle.text = it.title
            binding.tvReleaseDate.text = it.release_date
            binding.tvDescription.text = it.overview
            val path = "https://image.tmdb.org/t/p/original/${it.poster_path}"
            Glide.with(this)
                .load(path)
                .fitCenter()
                .into(binding.ivMoviePoster)

        }

        return binding.root
    }


}