package com.rockderick.moviebox.home.data.repository

import com.rockderick.moviebox.home.data.service.TheMovieDBAPI
import com.rockderick.moviebox.common.network.APIServiceAutoBuilder
import com.rockderick.moviebox.common.network.ApiServiceState
import com.rockderick.moviebox.home.data.model.Movies
import kotlinx.coroutines.flow.Flow

class MovieRepository {
   companion object {
       suspend fun fetchNowPlayingMovies(
       ): Flow<ApiServiceState<Movies>>? {
           val api = TheMovieDBAPI.Builder(null).build()
           return APIServiceAutoBuilder.executeEndPoint { api.getNowPlayingMovies() }

       }

       suspend fun fetchPopularMovies(
       ): Flow<ApiServiceState<Movies>>? {
           val api = TheMovieDBAPI.Builder(null).build()
           return APIServiceAutoBuilder.executeEndPoint { api.getPopularMovies() }

       }

   }

}