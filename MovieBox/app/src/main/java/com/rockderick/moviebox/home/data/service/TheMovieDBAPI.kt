package com.rockderick.moviebox.home.data.service



import com.rockderick.moviebox.common.network.APIServiceAutoBuilder
import com.rockderick.moviebox.common.network.BaseUrl
import com.rockderick.moviebox.home.data.model.Movies
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query



interface TheMovieDBAPI {

    @GET("/3/movie/now_playing")
    suspend fun getNowPlayingMovies(
        @Query("api_key") key: String = BaseUrl.GetBaseUrl().api_key,
        @Query("language") language: String = "en-US"
    ): Movies

    @GET("/3/movie/popular")
    suspend fun getPopularMovies(
        @Query("api_key") key: String = BaseUrl.GetBaseUrl().api_key,
        @Query("language") language: String = "en-US"
    ): Movies

    @GET("/3/movie/{id}")
    fun getDetails(
            @Path("id") id: Int,
            @Query("api_key") apiKey: String = BaseUrl.GetBaseUrl().api_key,
            @Query("language") language: String="en-US"
            ): Movies


    class Builder(headers: Map<String, String>?) :
        APIServiceAutoBuilder<TheMovieDBAPI>(headers) {
        override fun build(): TheMovieDBAPI {
            return client.create(TheMovieDBAPI::class.java)
        }
    }
}