package com.rockderick.moviebox.home.view

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.rockderick.moviebox.R
import com.rockderick.moviebox.databinding.FragmentHomeBinding
import com.rockderick.moviebox.home.data.model.Results
import com.rockderick.moviebox.home.view.adapter.MoviesAdapter
import com.rockderick.moviebox.home.view.adapter.PageAdapter
import com.rockderick.moviebox.home.viewmodel.HomeViewModel

class HomeFragment : Fragment() {

    private lateinit var viewModel: HomeViewModel
    private lateinit var binding:  FragmentHomeBinding
    private lateinit var adapterData: MutableList<PageAdapter.Page>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home,container,false)

        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)

        binding.recyclerView.layoutManager =
            LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        var moviesAdapter = MoviesAdapter( ArrayList<Results>())
        binding.recyclerView.adapter = moviesAdapter



        viewModel.playinNowMovies.observe(viewLifecycleOwner, Observer {
            moviesAdapter!!.items = it
            moviesAdapter.notifyDataSetChanged()

        })

        viewModel.popularMovies.observe(viewLifecycleOwner, Observer {
            adapterData =  MutableList(it.size) { i ->
                PageAdapter.Page(it[i])
            }

            val pageAdapter = PageAdapter().apply {
                setData(adapterData)
            }

            binding.carousel.adapter = pageAdapter


            pageAdapter.setData(adapterData)

        })


        return binding.root
    }



}