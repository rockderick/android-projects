package com.rockderick.moviebox.home.view.adapter

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.rockderick.moviebox.R
import com.rockderick.moviebox.home.data.model.Results
import com.rockderick.moviebox.home.view.HomeFragmentDirections
import io.github.vejei.carouselview.CarouselAdapter

class PageAdapter : CarouselAdapter<PageAdapter.ViewHolder>() {
    private var data = mutableListOf<Page>()

    fun setData(data: MutableList<Page>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    data class Page(val results: Results)

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val backgroundImageView = itemView.findViewById<ImageView>(R.id.view_background)

        fun bind(page: Page)  = with(itemView){
            backgroundImageView.clipToOutline = true
            val path = "https://image.tmdb.org/t/p/original/${page.results.poster_path}"
            Glide.with(this)
                .load(path)
                .fitCenter()
                .into(backgroundImageView)


            itemView.setOnClickListener {
                val action = HomeFragmentDirections.actionHomeFragmentToDetailsFragment(page.results)
                findNavController().navigate(action)
            }

        }




    }

    override fun onCreatePageViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_page, parent, false)
        )
    }

    override fun onBindPageViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getPageCount(): Int {
        return data.size
    }
}