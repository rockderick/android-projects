package com.rockderick.moviebox.home.view.custom


import android.animation.ObjectAnimator
import android.animation.TimeInterpolator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import com.rockderick.moviebox.R


class RatingView(private val mContext: Context, attrs: AttributeSet?) :
    View(mContext, attrs) {
    private var mPercent = 75f
    private var mStrokeWidth = 0f
    private var mBgColor = -0x1e1e1f
    private var mStartAngle = 0f
    private var mFgColorStart = -0x1c00
    private var mFgColorEnd = -0xb800
    private var mShader: LinearGradient? = null
    private var mOval: RectF? = null
    private var mPaint: Paint? = null
    private var animator: ObjectAnimator? = null
    private fun init() {
        mPaint = Paint()
        mPaint!!.isAntiAlias = true
        mPaint!!.style = Paint.Style.STROKE
        mPaint!!.strokeWidth = mStrokeWidth
        mPaint!!.strokeCap = Paint.Cap.ROUND
    }

    private fun dp2px(dp: Float): Int {
        return (mContext.resources.displayMetrics.density * dp + 0.5f).toInt()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        mPaint!!.shader = null
        mPaint!!.color = mBgColor
        canvas.drawArc(mOval!!, 0f, 360f, false, mPaint!!)
        mPaint!!.shader = mShader
        canvas.drawArc(mOval!!, mStartAngle, mPercent * 3.6f, false, mPaint!!)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        updateOval()
        mShader = LinearGradient(
            mOval!!.left, mOval!!.top,
            mOval!!.left, mOval!!.bottom, mFgColorStart, mFgColorEnd, Shader.TileMode.MIRROR
        )
    }

    var percent: Float
        get() = mPercent
        set(mPercent) {
            this.mPercent = mPercent
            refreshTheLayout()
        }
    var strokeWidth: Float
        get() = mStrokeWidth
        set(mStrokeWidth) {
            this.mStrokeWidth = mStrokeWidth
            mPaint!!.strokeWidth = mStrokeWidth
            updateOval()
            refreshTheLayout()
        }

    private fun updateOval() {
        val xp = paddingLeft + paddingRight
        val yp = paddingBottom + paddingTop
        mOval = RectF(
            paddingLeft + mStrokeWidth, paddingTop + mStrokeWidth,
            paddingLeft + (width - xp) - mStrokeWidth,
            paddingTop + (height - yp) - mStrokeWidth
        )
    }

    fun setStrokeWidthDp(dp: Float) {
        mStrokeWidth = dp2px(dp).toFloat()
        mPaint!!.strokeWidth = mStrokeWidth
        updateOval()
        refreshTheLayout()
    }

    fun refreshTheLayout() {
        invalidate()
        requestLayout()
    }

    var fgColorStart: Int
        get() = mFgColorStart
        set(mFgColorStart) {
            this.mFgColorStart = mFgColorStart
            mShader = LinearGradient(
                mOval!!.left, mOval!!.top,
                mOval!!.left, mOval!!.bottom, mFgColorStart, mFgColorEnd, Shader.TileMode.MIRROR
            )
            refreshTheLayout()
        }
    var fgColorEnd: Int
        get() = mFgColorEnd
        set(mFgColorEnd) {
            this.mFgColorEnd = mFgColorEnd
            mShader = LinearGradient(
                mOval!!.left, mOval!!.top,
                mOval!!.left, mOval!!.bottom, mFgColorStart, mFgColorEnd, Shader.TileMode.MIRROR
            )
            refreshTheLayout()
        }
    var startAngle: Float
        get() = mStartAngle
        set(mStartAngle) {
            this.mStartAngle = mStartAngle + 270
            refreshTheLayout()
        }

    @JvmOverloads
    fun animateIndeterminate(
        durationOneCircle: Int = 800,
        interpolator: TimeInterpolator? = AccelerateDecelerateInterpolator()
    ) {
        animator = ObjectAnimator.ofFloat(this, "startAngle", startAngle, startAngle + 360)
        if (interpolator != null) animator?.setInterpolator(interpolator)
        animator?.setDuration(durationOneCircle.toLong())
        animator?.setRepeatCount(ValueAnimator.INFINITE)
        animator?.setRepeatMode(ValueAnimator.RESTART)
        animator?.start()
    }

    fun stopAnimateIndeterminate() {
        if (animator != null) {
            animator!!.cancel()
            animator = null
        }
    }

    init {
        val a = mContext.theme.obtainStyledAttributes(
            attrs,
            R.styleable.RatingView,
            0, 0
        )
        try {
            mBgColor = a.getColor(R.styleable.RatingView_bgColor, -0x1e1e1f)
            mFgColorEnd = a.getColor(R.styleable.RatingView_fgColorEnd, -0xb800)
            mFgColorStart = a.getColor(R.styleable.RatingView_fgColorStart, -0x1c00)
            mPercent = a.getFloat(R.styleable.RatingView_percent, 75f)
            mStartAngle = a.getFloat(R.styleable.RatingView_startAngle, 0f) + 270
            mStrokeWidth = a.getDimensionPixelSize(
                R.styleable.RatingView_strokeWidth,
                dp2px(21f)
            ).toFloat()
        } finally {
            a.recycle()
        }
        init()
    }
}