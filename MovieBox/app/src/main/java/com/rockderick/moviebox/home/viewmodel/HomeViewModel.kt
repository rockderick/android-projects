package com.rockderick.moviebox.home.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rockderick.moviebox.common.network.ApiServiceState
import com.rockderick.moviebox.home.data.model.Movies
import com.rockderick.moviebox.home.data.model.Results
import com.rockderick.moviebox.home.data.repository.MovieRepository
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class HomeViewModel : ViewModel() {
    // TODO: Implement the ViewModel

    var popularMovies  = MutableLiveData<ArrayList<Results>>()
    var playinNowMovies  = MutableLiveData<ArrayList<Results>>()




    init {
        viewModelScope.launch {

            MovieRepository.fetchNowPlayingMovies()?.onEach {
                if (it is ApiServiceState.Success<Movies>) {
                   Log.i("Peliculas","Now playing ${it.data.results}")
                    playinNowMovies.value = it.data.results as ArrayList<Results>
                }
                if (it is ApiServiceState.Error<*>) {
                    Log.i("Peliculas","now playing Error")
                }
            }?.launchIn(viewModelScope)

        }

        viewModelScope.launch {

            MovieRepository.fetchPopularMovies()?.onEach {
                if (it is ApiServiceState.Success<Movies>) {
                    Log.i("Peliculas","Popular: ${it.data.results}")
                    popularMovies.value = it.data.results as ArrayList<Results>
                }
                if (it is ApiServiceState.Error<*>) {
                    Log.i("Peliculas","popular Error")
                }
            }?.launchIn(viewModelScope)

        }

        /*fun fetchMainScreenMovies() =liveData(Dispatchers.IO) {
            MovieRepository.fetchNowPlayingMovies()?.onEach {
                if(it is ApiServiceState.Loading){
                    emit(it)
                }
                if (it is ApiServiceState.Success<Movies>) {
                    Log.i("Peliculas","${it.data.results}")
                    emit(it)
                }
                if (it is ApiServiceState.Error<*>) {
                   emit(it)
                }
            }?.launchIn(viewModelScope)

        }*/

    }
}