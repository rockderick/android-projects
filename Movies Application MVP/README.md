MovieBox Application
=========================================



Introduction
------------

This is an application that use TheMovieDB API in order to visualize the currently playing movies
and the most popular movies f an application to visualize 


What the app does
--------------------

1. List horizontally currently playing movies.
2. Display the most popular movies in the vertical list view, with multiple pages.
3. When a user clicks on any movie list item, navigates to a detailed screen, with more information about the movie.



Notes
--------------------
1. Architecture: MVP
2. Glide for image caching
3. Third party libraries for carrousel


Screenshots
-----------

<img src="screenshots/screenshoot.gif" height="400" alt="Screenshot"/>



