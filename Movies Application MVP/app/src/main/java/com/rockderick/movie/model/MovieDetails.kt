package com.rockderick.movie.model



data class MovieDetails(
    val id: Int,
    val overview: String,
    val poster_path: String,
    val release_date: String,
    val title: String,
    val genres: List<Genre>
)

data class Genre(
    val id: Int,
    val name: String
)