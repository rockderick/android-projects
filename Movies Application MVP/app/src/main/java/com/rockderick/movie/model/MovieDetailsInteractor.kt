package com.rockderick.movie.model

interface MovieDetailsInteractor {

    fun getMoviesDetailsAPI(id: Int)
}