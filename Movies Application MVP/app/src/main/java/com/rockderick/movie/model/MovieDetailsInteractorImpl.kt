package com.rockderick.movie.model

import com.rockderick.movie.presenter.MovieDetailsPresenter

class MovieDetailsInteractorImpl(var movieDetailsPresenter: MovieDetailsPresenter):MovieDetailsInteractor {

    private var movieDetailsRepository : MovieDetailsRepository = MovieDetailsRepositoryImpl(movieDetailsPresenter)

    override fun getMoviesDetailsAPI(id: Int) {
       movieDetailsRepository.getMoviesDetailsAPI(id)
    }
}