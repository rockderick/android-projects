package com.rockderick.movie.model

interface MovieDetailsRepository {

    fun getMoviesDetailsAPI(id: Int)
}