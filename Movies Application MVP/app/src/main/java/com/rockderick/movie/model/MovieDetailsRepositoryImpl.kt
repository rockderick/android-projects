package com.rockderick.movie.model

import com.rockderick.movie.presenter.MovieDetailsPresenter
import com.rockderick.movie.utilities.TheMovieDBAPI
import com.rockderick.assignment.ui.movie.utilities.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MovieDetailsRepositoryImpl(var movieDetailsPresenter: MovieDetailsPresenter): MovieDetailsRepository {

    val request = ServiceBuilder.buildService(TheMovieDBAPI::class.java)


    override fun getMoviesDetailsAPI(id: Int) {

        val call = request.getDetails(id,"55957fcf3ba81b137f8fc01ac5a31fb5")

        call.enqueue(object : Callback<MovieDetails> {

            override fun onResponse(call: Call<MovieDetails>, response: Response<MovieDetails>) {

                if (response.isSuccessful){
                    var result = response.body()!!


                    movieDetailsPresenter.showMovieInDetail(result)
                }
            }
            override fun onFailure(call: Call<MovieDetails>, t: Throwable) {
                println("result error" )
            }
        })
    }
}