package com.rockderick.assignment.ui.movie.model

interface MovieInteractor {

    fun getMoviesAPI()
}