package com.rockderick.assignment.ui.movie.model

import com.rockderick.assignment.ui.movie.presenter.MoviePresenter

class MovieInteractorImpl(var moviePresenter: MoviePresenter):MovieInteractor {


    private var movieRepository :MovieRepository = MovieRepositoryImpl(moviePresenter)

    override fun getMoviesAPI(){
        movieRepository.getMoviesAPI()
    }
}