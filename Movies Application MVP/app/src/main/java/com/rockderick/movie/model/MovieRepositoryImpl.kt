package com.rockderick.assignment.ui.movie.model

import com.rockderick.movie.utilities.TheMovieDBAPI
import com.rockderick.assignment.ui.movie.presenter.MoviePresenter

import com.rockderick.assignment.ui.movie.utilities.ServiceBuilder

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieRepositoryImpl(var moviePresenter: MoviePresenter): MovieRepository {


    private val yourKey = "55957fcf3ba81b137f8fc01ac5a31fb5"
    var movies: ArrayList<NowPlayingMovies> = ArrayList()
    //private  lateinit  var jsonArray: JsonArray

    val request = ServiceBuilder.buildService(TheMovieDBAPI::class.java)
    val call = request.getNowPlayingMovies(yourKey)
    val call1 = request.getPopularMovies(yourKey)


    override fun getMoviesAPI(){



        call.enqueue(object : Callback<NowPlayingMovies> {

            override fun onResponse(call: Call<NowPlayingMovies>, response: Response<NowPlayingMovies>) {

                if (response.isSuccessful){
                    var results = response.body()!!.results as ArrayList
                    moviePresenter.showPlayingNowMovies(results)
                }
            }
            override fun onFailure(call: Call<NowPlayingMovies>, t: Throwable) {
                println("result error" )
            }
        })


        call1.enqueue(object : Callback<PopularMovies> {

            override fun onResponse(call: Call<PopularMovies>, response: Response<PopularMovies>) {

                if (response.isSuccessful){
                    var results = response.body()!!.results as ArrayList

                    moviePresenter.showPopularMovies(results)

                }
            }
            override fun onFailure(call: Call<PopularMovies>, t: Throwable) {
                println("result error" )
            }
        })



    }
}