package com.rockderick.movie.presenter

import com.rockderick.movie.model.MovieDetails

interface MovieDetailsPresenter {

    //Interactor
    fun getMovieDetail(id: Int)
    //View
    fun showMovieInDetail(movieDetails: MovieDetails)
}