package com.rockderick.movie.presenter

import com.rockderick.movie.model.MovieDetails
import com.rockderick.movie.model.MovieDetailsInteractor
import com.rockderick.movie.model.MovieDetailsInteractorImpl
import com.rockderick.view.MovieDetailsView


class MovieDetailsPresenterImpl(var movieDetailsView: MovieDetailsView):MovieDetailsPresenter {

    private var movieDetailsInteractor : MovieDetailsInteractor = MovieDetailsInteractorImpl(this)


    override fun getMovieDetail(id: Int) {
        return movieDetailsInteractor.getMoviesDetailsAPI(id)

    }

    override fun showMovieInDetail(movieDetails: MovieDetails) {
        movieDetailsView.showMovieInDetail(movieDetails)
    }
}