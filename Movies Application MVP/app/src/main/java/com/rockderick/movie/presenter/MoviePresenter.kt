package com.rockderick.assignment.ui.movie.presenter

import com.rockderick.assignment.ui.movie.model.Result

interface MoviePresenter {

    //Interactor
    fun getMovies()
    //View
    fun showPlayingNowMovies(playingNowMovies: ArrayList<Result>)
    fun showPopularMovies(popularMovies: ArrayList<Result>)

}