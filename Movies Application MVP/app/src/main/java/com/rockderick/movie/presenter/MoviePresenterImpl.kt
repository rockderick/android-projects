package com.rockderick.assignment.ui.movie.presenter

import com.rockderick.assignment.ui.movie.model.MovieInteractor
import com.rockderick.assignment.ui.movie.model.MovieInteractorImpl
import com.rockderick.assignment.ui.movie.model.Result
import com.rockderick.view.MovieView

class MoviePresenterImpl(var movieView: MovieView):MoviePresenter{

    private var movieInteractor : MovieInteractor = MovieInteractorImpl(this)

    override fun getMovies(){
        return movieInteractor.getMoviesAPI()
    }

    override fun showPlayingNowMovies(popularMovies: ArrayList<Result>) {
        movieView.showPlayingNowMovies(popularMovies)
    }

    override fun showPopularMovies(popularMovies: ArrayList<Result>) {
        movieView.showPopularMovies(popularMovies)
    }

    //override fun showMovies(jsonArray: JsonArray) {
         // movieView.showMovies(jsonArray)
    //}
}