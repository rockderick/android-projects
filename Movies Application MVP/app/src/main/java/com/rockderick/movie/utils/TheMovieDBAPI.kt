package com.rockderick.movie.utilities


import com.rockderick.movie.model.MovieDetails
import com.rockderick.assignment.ui.movie.model.NowPlayingMovies
import com.rockderick.assignment.ui.movie.model.PopularMovies
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TheMovieDBAPI {

    @GET("/3/movie/now_playing")
    fun getNowPlayingMovies(
        @Query("api_key") key: String,
        @Query("language") language: String = "en-US"
    ): Call<NowPlayingMovies>

    @GET("/3/movie/popular")
    fun getPopularMovies(
        @Query("api_key") key: String,
        @Query("language") language: String = "en-US"
    ): Call<PopularMovies>

    @GET("/3/movie/{id}")
    fun getDetails(
            @Path("id") id: Int,
            @Query("api_key") apiKey: String?,
            @Query("language") language: String="en-US"
            ): Call<MovieDetails>
}