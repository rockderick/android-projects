package com.rockderick.view

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rockderick.assignment.R

import com.rockderick.view.movie.MoviesAdapter
import com.rockderick.assignment.ui.movie.model.Result

import com.rockderick.assignment.ui.movie.presenter.MoviePresenter
import com.rockderick.assignment.ui.movie.presenter.MoviePresenterImpl
import com.rockderick.view.movie.PageAdapter
import com.google.gson.JsonParser
import io.github.vejei.carouselview.CarouselView
import java.net.URL


class MainActivity : AppCompatActivity(), MovieView {

    private val baseUrl = "https://api.themoviedb.org/3"
    private val yourKey = "03ac1bb2cf9b74661d41fbca8087307b"

    private lateinit var moviesAdapter: MoviesAdapter
    private lateinit var recyclerView: RecyclerView
    private lateinit var sideBySidePreviewScaleCarouselView: CarouselView
    private lateinit var progressBar: ProgressBar

    lateinit var adapterData: MutableList<PageAdapter.Page>



    private var moviePresenter: MoviePresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()

        progressBar = findViewById(R.id.progress_bar)

        recyclerView = findViewById(R.id.recyclerView)
        recyclerView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        var results = ArrayList<Result>()
        moviesAdapter = MoviesAdapter(results)
        recyclerView.adapter = moviesAdapter

        moviePresenter = MoviePresenterImpl(this)

        sideBySidePreviewScaleCarouselView = findViewById<CarouselView>(
            R.id.carousel_view_side_by_side_preview_scale)

        getMovies()


    }

    private fun fetchMovies() {
        val jsonString =
            URL("$baseUrl/movie/now_playing?language=en-US&page=undefined&api_key=$yourKey").readText()
        val jsonObject = JsonParser.parseString(jsonString).asJsonObject
        // moviesAdapter.items = jsonObject["results"] as JsonArray
        moviesAdapter.notifyDataSetChanged()
    }



    override fun getMovies() {
        moviePresenter!!.getMovies()
    }

    override fun showPlayingNowMovies(nowPlayingMovies: ArrayList<Result>) {

        adapterData =  MutableList(nowPlayingMovies.size) { i ->
            PageAdapter.Page(
                nowPlayingMovies.get(i).id,
                nowPlayingMovies.get(i).poster_path
            )
        }

        val pageAdapter = PageAdapter().apply {
            setData(adapterData)
        }

        sideBySidePreviewScaleCarouselView.adapter = pageAdapter


        pageAdapter.setData(adapterData)

        //moviesAdapter!!.items = nowPlayingMovies
        //moviesAdapter.notifyDataSetChanged()


    }

    override fun showPopularMovies(popularMovies: ArrayList<Result>) {
        moviesAdapter!!.items = popularMovies
        moviesAdapter.notifyDataSetChanged()
        progressBar.visibility = View.GONE
    }


}


