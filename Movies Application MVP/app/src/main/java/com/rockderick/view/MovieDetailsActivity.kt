package com.rockderick.view

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rockderick.assignment.R
import com.rockderick.movie.model.MovieDetails
import com.rockderick.movie.presenter.MovieDetailsPresenter
import com.rockderick.movie.presenter.MovieDetailsPresenterImpl
import com.rockderick.view.movie.GenreAdapter
import com.bumptech.glide.Glide


class MovieDetailsActivity : AppCompatActivity(),MovieDetailsView {


    private var movieDetailsPresenter: MovieDetailsPresenter? = null
    private lateinit var tvMovieTitle:TextView
    private lateinit var tvMovieDescription:TextView
    private lateinit var tvReleaseDate:TextView
    private lateinit var ivMoviePoster: ImageView
    private lateinit var rvGenre: RecyclerView
    private lateinit var myAdapter:GenreAdapter
    private lateinit var data: Array<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detalis)
        supportActionBar?.hide()

        tvMovieTitle = findViewById(R.id.tvTitle)
        tvMovieDescription = findViewById(R.id.tvDescription)
        tvReleaseDate = findViewById(R.id.tvReleaseDate)
        ivMoviePoster = findViewById(R.id.ivMoviePoster)
        rvGenre = findViewById(R.id.lvGenre)

        movieDetailsPresenter= MovieDetailsPresenterImpl(this)

        rvGenre.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

        val id = intent.getSerializableExtra("ID") as Int
        getMovieDetail(id)


    }

    override fun getMovieDetail(id: Int) {
        movieDetailsPresenter?.getMovieDetail(id)
    }

    override fun showMovieInDetail(movieDetails: MovieDetails) {
        tvMovieTitle.setText(movieDetails.title)
        tvMovieDescription.setText(movieDetails.overview)
        tvReleaseDate.setText(movieDetails.release_date)
        val path = "https://image.tmdb.org/t/p/original/${movieDetails.poster_path}"
        Glide.with(this)
            .load(path)
            .fitCenter()
            .into(ivMoviePoster)
        data = Array(movieDetails.genres.size, { i -> movieDetails.genres[i].name })

        var adapter = GenreAdapter(data)
        rvGenre.adapter = adapter
    }
}


