package com.rockderick.view

import com.rockderick.movie.model.MovieDetails

interface MovieDetailsView {

    //Presenter
    fun getMovieDetail(id: Int)
    //View
    fun showMovieInDetail(movieDetail: MovieDetails)

}