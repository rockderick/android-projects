package com.rockderick.view


import com.rockderick.assignment.ui.movie.model.Result

interface MovieView {

    //Presenter
    fun getMovies()
    //View
    fun showPlayingNowMovies(nowPlayingMovies: ArrayList<Result>)
    fun showPopularMovies(popularMovies: ArrayList<Result>)

}