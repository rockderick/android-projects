package com.rockderick.view.movie


import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

import com.rockderick.assignment.R

import com.bumptech.glide.Glide
import com.rockderick.assignment.ui.movie.model.Result
import com.rockderick.view.MovieDetailsActivity
import com.rockderick.view.custom.RatingView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import kotlin.math.roundToInt


class MoviesAdapter(var items: ArrayList<Result>) :
    RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.movie_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var movie = items.get(position)
        holder.bind(movie)

    }
    //holder.bind(items[position] as JsonObject)

    override fun getItemCount() = items.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)  {
        lateinit var poster: ImageView
        lateinit var title: TextView
        lateinit var releaseDate: TextView
        lateinit var rating: RatingView
        lateinit var percentText: TextView
        lateinit var id: Integer

        fun bind(item: Result) = with(itemView) {

            id = item.id
            poster = itemView.findViewById(R.id.poster)

            Glide.with(this)
                .load("https://image.tmdb.org/t/p/original/${item.poster_path}")
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .into(poster)

            Log.v("movies", "https://image.tmdb.org/t/p/original/${item.poster_path}")

            title = itemView.findViewById(R.id.title)
            title.text = item.title

            releaseDate = itemView.findViewById(R.id.releaseDate)
            releaseDate.text = item.release_date

            rating = itemView.findViewById(R.id.crpv)
            rating.percent = (item.vote_average * 10).toFloat()


            percentText = itemView.findViewById(R.id.tvPercent)
            var score = (item.vote_average * 10).roundToInt()
            percentText.text = "${score}%"


            itemView.setOnClickListener {
                Log.i("CLICK Coupon: ", id.toString())
                val movieDetailsIntent = Intent(context, MovieDetailsActivity::class.java)
                movieDetailsIntent.putExtra("ID", id)
                context?.startActivity(movieDetailsIntent)
            }
        }


    }
}

