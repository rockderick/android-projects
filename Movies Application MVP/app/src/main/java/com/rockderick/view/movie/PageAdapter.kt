package com.rockderick.view.movie

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.rockderick.assignment.R
import com.rockderick.view.MovieDetailsActivity
import com.bumptech.glide.Glide
import io.github.vejei.carouselview.CarouselAdapter

class PageAdapter : CarouselAdapter<PageAdapter.ViewHolder>() {
    private var data = mutableListOf<Page>()

    fun setData(data: MutableList<Page>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    data class Page(val id:Int, val path: String)

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val backgroundImageView = itemView.findViewById<ImageView>(R.id.view_background)

        fun bind(page: Page)  = with(itemView){
            backgroundImageView.clipToOutline = true
            val path = "https://image.tmdb.org/t/p/original/${page.path}"
            Glide.with(this)
                .load(path)
                .fitCenter()
                .into(backgroundImageView)
            //backgroundImageView.setImageResource(page.imageRes)
            //contentTextView.text = page.content

            itemView.setOnClickListener {

                val movieDetailsIntent = Intent(context, MovieDetailsActivity::class.java)
                movieDetailsIntent.putExtra("ID", page.id)
                context?.startActivity(movieDetailsIntent)
            }

        }




    }

    override fun onCreatePageViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_page, parent, false)
        )
    }

    override fun onBindPageViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getPageCount(): Int {
        return data.size
    }
}