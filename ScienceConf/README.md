ScienceConf (Android-Kotlin)
=========================================



Introduction
------------

This is an application to visualize the data of an application to visualize 
the most important data about a generic conference (ScienceConf).

What the app does
--------------------
This app show the following concepts:
1. How to organize an app using the MVVM software architectural pattern applied to a generic conference app.
2. Extract data from a non-retational database such as Cloudfirestore.
3. Applied some concepts from android jetpack such as the Navigation component.
4. Use of the google maps API.

Screenshots
-----------

<img src="screenshots/20201207_171101.gif" height="400" alt="Screenshot"/>



