package com.rockderick.scienceconf.model

import java.util.*
import  java.io.Serializable

class Conference: Serializable {
    var title: String = ""
    var description: String = ""
    var tag: String = ""
    var datetime: Date = Date()
    var speaker: String = ""

}