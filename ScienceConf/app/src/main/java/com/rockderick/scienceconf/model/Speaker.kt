package com.rockderick.scienceconf.model

import java.io.Serializable


class Speaker: Serializable {
    var name = ""
    var jobtitle= ""
    var workPlace = ""
    var biography = ""
    var twitter = ""
    var image = ""
    var category = 0

}