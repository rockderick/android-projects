package com.rockderick.scienceconf.model

class Ubication {
    val name = "CERN"
    val address = "Espl. des Particules 1, 1211 Meyrin, Switzerland"
    val latitud = 46.2344
    val longitud = 6.04594
    val phone = "+52 55 1519 4670"
    val website = "https://home.cern/"
    val photo = "https://lh5.googleusercontent.com/p/AF1QipPj-rZ_uZmMzvQT3iVlA832hei26T_qjFbjYiW6=w408-h270-k-no"
}