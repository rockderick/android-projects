package com.rockderick.scienceconf.view.adapter

import com.rockderick.scienceconf.model.Conference


interface ScheduleListener {
    fun onConferenceClicked(conference: Conference, position: Int)
}