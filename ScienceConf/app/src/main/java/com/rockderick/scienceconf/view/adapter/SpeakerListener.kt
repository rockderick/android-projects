package com.rockderick.scienceconf.view.adapter

import com.rockderick.scienceconf.model.Speaker


interface SpeakerListener {
    fun onSpeakerClicked(speaker: Speaker, position: Int)
}