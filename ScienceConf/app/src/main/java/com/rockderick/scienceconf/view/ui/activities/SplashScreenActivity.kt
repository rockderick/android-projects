package com.rockderick.scienceconf.view.ui.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.rockderick.scienceconf.R
import com.rockderick.scienceconf.view.MainActivity
import kotlinx.android.synthetic.main.activity_splash_screen.*

class SplashScreenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        val anim = AnimationUtils.loadAnimation(this, R.anim.animation)
        ivLogoScienceConf.startAnimation(anim)

        val intent = Intent(this,MainActivity::class.java)

        anim.setAnimationListener(object: Animation.AnimationListener{
            override fun onAnimationStart(animation: Animation?) {

            }

            override fun onAnimationEnd(animation: Animation?) {
                  startActivity(intent)
                  finish()
            }

            override fun onAnimationRepeat(animation: Animation?) {

            }

        }   )
    }
}