package com.rockderick.drinksapp

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.rockderick.drinksapp.domain.DrinkDao
import com.rockderick.drinksapp.domain.model.DrinkEntity


@Database(entities = arrayOf(DrinkEntity::class),version= 3,exportSchema = false)
abstract class AppDataBase: RoomDatabase() {

    abstract  fun drinkDao(): DrinkDao


}