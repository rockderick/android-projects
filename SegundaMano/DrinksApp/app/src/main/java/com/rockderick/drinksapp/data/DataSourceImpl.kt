package com.rockderick.drinksapp.data

import com.rockderick.drinksapp.AppDataBase
import com.rockderick.drinksapp.domain.DataSource
import com.rockderick.drinksapp.domain.DrinkDao
import com.rockderick.drinksapp.domain.model.Drink
import com.rockderick.drinksapp.domain.model.DrinkEntity
import com.rockderick.drinksapp.domain.network.CocktailApi
import com.rockderick.drinksapp.vo.Resource
import javax.inject.Inject

class DataSourceImpl @Inject constructor( private val drinkDao: DrinkDao):DataSource {


    override suspend fun getDrinkByName(drinkName: String):Resource<List<Drink>>{
        return Resource.Success(CocktailApi.retrofitService.getDrinkByName(drinkName).drinkList)
    }



    override suspend fun insertDrinkIntoRoom(drink: DrinkEntity){
         drinkDao.insertFavorite(drink)
    }

    override suspend fun getTragosFavoritos(): Resource<List<DrinkEntity>> {
        return Resource.Success(drinkDao.getAllFavoriteDrinks())
    }

    override suspend fun deleteDrink(drink: DrinkEntity) {
        drinkDao.deleteDrink(drink)
    }


}