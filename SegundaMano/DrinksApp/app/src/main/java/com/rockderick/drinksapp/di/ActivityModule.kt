package com.rockderick.drinksapp.di

import com.rockderick.drinksapp.data.DataSourceImpl
import com.rockderick.drinksapp.domain.DataSource
import com.rockderick.drinksapp.domain.Repo
import com.rockderick.drinksapp.domain.RepoImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
abstract class ActivityModule {

    @Binds
    abstract fun bindRepoImpl(repoImpl: RepoImpl): Repo

    @Binds
    abstract fun bindDatasourceImpl(dataSourceImpl: DataSourceImpl): DataSource
}