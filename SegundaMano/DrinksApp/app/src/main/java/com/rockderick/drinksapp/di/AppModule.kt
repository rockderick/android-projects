package com.rockderick.drinksapp.di

import android.content.Context
import androidx.room.Room
import com.rockderick.drinksapp.AppDataBase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideRoomInstance(@ApplicationContext context: Context)=
        Room.databaseBuilder(
                context.applicationContext,
                AppDataBase::class.java,
                "table_drinks" ).fallbackToDestructiveMigration()
            .build()


    @Singleton
    @Provides
    fun provideDrinkDao(db:AppDataBase)=
            db.drinkDao()

}