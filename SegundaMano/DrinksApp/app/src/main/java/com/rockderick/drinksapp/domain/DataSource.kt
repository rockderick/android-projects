package com.rockderick.drinksapp.domain

import com.rockderick.drinksapp.domain.model.Drink
import com.rockderick.drinksapp.domain.model.DrinkEntity
import com.rockderick.drinksapp.domain.network.CocktailApi
import com.rockderick.drinksapp.vo.Resource

interface DataSource {

    suspend fun getDrinkByName(drinkName: String): Resource<List<Drink>>

    suspend fun insertDrinkIntoRoom(drink: DrinkEntity)

    suspend fun getTragosFavoritos(): Resource<List<DrinkEntity>>

    suspend fun deleteDrink(drink: DrinkEntity)

}