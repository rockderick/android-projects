package com.rockderick.drinksapp.domain

import androidx.room.*
import com.rockderick.drinksapp.domain.model.Drink
import com.rockderick.drinksapp.domain.model.DrinkEntity

@Dao
interface DrinkDao {

    @Query("SELECT * FROM favoritesTable")
    suspend fun getAllFavoriteDrinks():List<DrinkEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFavorite(drink: DrinkEntity)

    @Delete
    suspend fun deleteDrink(drink: DrinkEntity)


}