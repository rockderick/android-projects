package com.rockderick.drinksapp.domain

import com.rockderick.drinksapp.domain.model.Drink
import com.rockderick.drinksapp.domain.model.DrinkEntity
import com.rockderick.drinksapp.vo.Resource

interface Repo {
    suspend fun getTragosList(drinkName: String):Resource<List<Drink>>
    suspend fun getTragosFavoritos(): Resource<List<DrinkEntity>>
    suspend fun insertTrago(trago: DrinkEntity)
    suspend fun deleteDrink(drink: DrinkEntity)
}