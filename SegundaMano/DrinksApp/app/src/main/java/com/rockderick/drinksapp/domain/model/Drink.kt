package com.rockderick.drinksapp.domain.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Drink(
                @Json(name = "idDrink")
                val cocktailId: String = "",
                @Json(name="strDrinkThumb")
                val image:String="",
                @Json(name="strDrink")
                val name: String="",
                @Json(name="strInstructions")
                val description: String ="",
                @Json(name="strAlcoholic")
                val hasAlcohol: String ="Non Alcoholic"
):Parcelable

data class  DrinkList( @Json(name="drinks")val drinkList: List<Drink>)

@Entity(tableName = "favoritesTable")
data class DrinkEntity(
    @PrimaryKey
    val drinkId: String,
    @ColumnInfo(name="drink_image")
    val image:String="",
    @ColumnInfo(name="drink_name")
    val name: String="",
    @ColumnInfo(name="drink_description")
    val description: String ="",
    @ColumnInfo(name="drink_has_alcohol")
    val hasAlcohol: String ="Non Alcoholic"

)