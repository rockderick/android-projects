package com.rockderick.drinksapp.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.github.chrisbanes.photoview.PhotoView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.rockderick.drinksapp.AppDataBase
import com.rockderick.drinksapp.R
import com.rockderick.drinksapp.data.DataSourceImpl
import com.rockderick.drinksapp.domain.RepoImpl
import com.rockderick.drinksapp.domain.model.Drink
import com.rockderick.drinksapp.domain.model.DrinkEntity
import com.rockderick.drinksapp.ui.viewmodel.MainViewModel
import com.rockderick.drinksapp.ui.viewmodel.VMFactory
import dagger.hilt.android.AndroidEntryPoint

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [DetailCocktailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */

@AndroidEntryPoint
class DetailCocktailFragment : Fragment() {

    private val viewModel by activityViewModels<MainViewModel> ()


    private lateinit var drink : Drink

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireArguments().let{
            drink = it.getParcelable<Drink>("drink")!!
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_cocktail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val imageViewDrink: PhotoView = view.findViewById(R.id.iv_drink)
        val textViewTitle: TextView = view.findViewById(R.id.tv_drinkTitle)
        val textViewHasAlcohol: TextView = view.findViewById(R.id.tvHasAlcohol)
        val textViewDesc: TextView = view.findViewById(R.id.tv_tragoDesc)
        val buttonSave : FloatingActionButton= view.findViewById(R.id.btn_save)

        Glide.with(requireContext()).load(drink.image).centerCrop().into(imageViewDrink)
        textViewTitle.text = drink.name
        textViewHasAlcohol.text = drink.hasAlcohol
        textViewDesc.text = drink.description

        buttonSave.setOnClickListener {
              viewModel.saveDrink(DrinkEntity(drink.cocktailId,drink.image,drink.name,drink.hasAlcohol))
              Toast.makeText(requireContext(),"Drink is saved to fovorites",Toast.LENGTH_LONG).show()
        }

    }


}