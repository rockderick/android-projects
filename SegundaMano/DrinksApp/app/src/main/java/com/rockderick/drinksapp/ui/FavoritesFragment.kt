package com.rockderick.drinksapp.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rockderick.drinksapp.AppDataBase
import com.rockderick.drinksapp.R
import com.rockderick.drinksapp.data.DataSourceImpl
import com.rockderick.drinksapp.domain.DrinkDao
import com.rockderick.drinksapp.domain.RepoImpl
import com.rockderick.drinksapp.domain.model.Drink
import com.rockderick.drinksapp.domain.model.DrinkEntity
import com.rockderick.drinksapp.ui.viewmodel.CocktailAdapter
import com.rockderick.drinksapp.ui.viewmodel.MainViewModel
import com.rockderick.drinksapp.ui.viewmodel.VMFactory
import com.rockderick.drinksapp.vo.Resource
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class FavoritesFragment : Fragment(),CocktailAdapter.OnDrinkClickListener {




    private val viewModel by activityViewModels<MainViewModel> ()


    private lateinit var recyclerViewDrinks: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorites, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView(view)
        setupObservers()
    }

    private fun setupRecyclerView(view: View) {
        recyclerViewDrinks = view.findViewById(R.id.rv_drinks_favorites)
        recyclerViewDrinks.layoutManager = LinearLayoutManager(requireContext())
        recyclerViewDrinks.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                DividerItemDecoration.VERTICAL
            )
        )

    }

    private  fun setupObservers(){
        viewModel.getTragosFavoritos().observe(viewLifecycleOwner, Observer {
                result ->
            when(result){
                is Resource.Loading ->{

                    //progressBar.visibility = View.VISIBLE
                }
                is Resource.Success ->{
                    //progressBar.visibility = View.GONE
                    var cocktailList = result.data.map {
                        Drink(it.drinkId,it.image,it.name,it.hasAlcohol)
                    }
                    recyclerViewDrinks.adapter = CocktailAdapter(requireContext(),cocktailList,this)
                }
                is Resource.Failure ->{
                    //progressBar.visibility = View.GONE
                    Toast.makeText(requireContext(),"An error has ocurred ${result.exception}",
                        Toast.LENGTH_LONG)

                }
            }
        })
    }

    override fun onDrinkClick(drink: Drink,position: Int) {
        viewModel.deleteDrink(DrinkEntity(drink.cocktailId,drink.image,drink.name,drink.hasAlcohol))
        recyclerViewDrinks.adapter?.notifyItemRemoved(position)
    }


}
