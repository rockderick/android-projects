package com.rockderick.drinksapp.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rockderick.drinksapp.AppDataBase
import com.rockderick.drinksapp.R
import com.rockderick.drinksapp.data.DataSourceImpl
import com.rockderick.drinksapp.domain.RepoImpl
import com.rockderick.drinksapp.domain.model.Drink
import com.rockderick.drinksapp.ui.viewmodel.CocktailAdapter
import com.rockderick.drinksapp.ui.viewmodel.MainViewModel
import com.rockderick.drinksapp.ui.viewmodel.VMFactory
import com.rockderick.drinksapp.vo.Resource
import dagger.hilt.android.AndroidEntryPoint

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MainCocktailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class MainCocktailFragment : Fragment(), CocktailAdapter.OnDrinkClickListener {

    private val viewModel by activityViewModels<MainViewModel> ()

    private lateinit var recyclerViewDrinks: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_cocktail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView(view)
        setupSearchView(view)
        //setupObservers()
        //recyclerViewDrinks.adapter = CocktailAdapter(requireContext())
        val progressBar: RelativeLayout = view.findViewById(R.id.progressBar)

        viewModel.fetchTragosList.observe(viewLifecycleOwner, Observer {
            result ->
            when(result){
                is Resource.Loading ->{

                   progressBar.visibility = View.VISIBLE
                }
                is Resource.Success ->{
                    progressBar.visibility = View.GONE
                    var cocktailList = result.data
                    recyclerViewDrinks.adapter = CocktailAdapter(requireContext(),cocktailList,this)
                }
                is Resource.Failure ->{
                    progressBar.visibility = View.GONE
                    Toast.makeText(requireContext(),"An error has ocurred ${result.exception}",Toast.LENGTH_LONG)

                }
            }
        })

        val buttonFavorites : Button = view.findViewById(R.id.btnFavorites)

        buttonFavorites.setOnClickListener { findNavController().navigate(R.id.action_mainCocktailFragment_to_favoritesFragment) }

    }

    private fun setupRecyclerView(view :View){
        recyclerViewDrinks = view.findViewById(R.id.rv_drinks)
        recyclerViewDrinks.layoutManager = LinearLayoutManager(requireContext())
        recyclerViewDrinks.addItemDecoration(DividerItemDecoration(requireContext(),DividerItemDecoration.VERTICAL))
    }

    override fun onDrinkClick(drink: Drink, position: Int) {
        val bundle = Bundle()
        bundle.putParcelable("drink",drink)
        findNavController().navigate(R.id.action_mainCocktailFragment_to_detailCocktailFragment,bundle)
    }

    private fun setupSearchView(view: View){
        var searchView: SearchView = view.findViewById(R.id.search_view)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                viewModel.setTrago(query!!)
                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                return false
            }

        })
    }


}