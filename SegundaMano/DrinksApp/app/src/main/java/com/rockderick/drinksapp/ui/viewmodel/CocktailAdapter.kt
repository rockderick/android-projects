package com.rockderick.drinksapp.ui.viewmodel

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.rockderick.drinksapp.R
import com.rockderick.drinksapp.base.BaseViewHolder
import com.rockderick.drinksapp.domain.model.Drink

class CocktailAdapter(private val context: Context, private  val cocktailList: List<Drink>,
                     private val itemClickListener: OnDrinkClickListener)
                     :RecyclerView.Adapter<BaseViewHolder<*>>() {

    interface OnDrinkClickListener{
        fun onDrinkClick(drink: Drink, position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        Log.v("Resultado1","Entra on create view")
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.drinks_item,parent,false))
    }


    //How to draw one element
    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        Log.v("Resultado1","Entra on bind")
        when(holder){
            is ViewHolder -> holder.bind(cocktailList[position],position)
        }
    }

    override fun getItemCount(): Int {
        return cocktailList.size
    }
    inner class ViewHolder(view: View) : BaseViewHolder<Drink>(view){

        private val tvDrink: TextView = itemView.findViewById(R.id.tv_drink)
        private val tvDescription: TextView = itemView.findViewById(R.id.tv_description)
        private val  imageView: ImageView = itemView.findViewById(R.id.img_drink)

        override fun bind(item: Drink,position: Int){
            tvDrink.text =item.name
            tvDescription.text = item.description
            Glide.with(context).load(item.image).into(imageView)

            itemView.setOnClickListener{itemClickListener.onDrinkClick(item,position)

            }
        }

    }
}