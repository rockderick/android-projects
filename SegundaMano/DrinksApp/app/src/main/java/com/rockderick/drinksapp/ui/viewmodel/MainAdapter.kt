package com.rockderick.drinksapp.ui.viewmodel

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.rockderick.drinksapp.R
import com.rockderick.drinksapp.base.BaseViewHolder
import com.rockderick.drinksapp.domain.model.Drink

class MainAdapter(private val context: Context, private val coktailList: List<Drink>):RecyclerView.Adapter<BaseViewHolder<*>> (){
    init {
        Log.v("Resultado1","Entra on init")
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        Log.v("Resultado1","Entra on create view")
        return MainViewHolder(LayoutInflater.from(context).inflate(R.layout.drinks_item,parent,false))
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        Log.v("Resultado1","Entra on bind")
         when(holder){
             is MainViewHolder -> holder.bind(coktailList[position],position)
         }
    }

    override fun getItemCount(): Int {
        return coktailList.size
    }

    inner class MainViewHolder(itemView: View):BaseViewHolder<Drink>(itemView){
        override fun bind(item: Drink, position: Int) {


            val tvDrink: TextView = itemView.findViewById(R.id.tv_drink)
            val tvDescription: TextView = itemView.findViewById(R.id.tv_description)
            val  imageView: ImageView = itemView.findViewById(R.id.img_drink)

            Glide.with(context).load(item.image).into(imageView)
            Log.v("Resultado1",item.name)
            tvDrink.text = item.name
            tvDescription.text = item.description

        }
    }
}