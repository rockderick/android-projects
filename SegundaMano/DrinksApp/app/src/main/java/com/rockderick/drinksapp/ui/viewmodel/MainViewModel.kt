package com.rockderick.drinksapp.ui.viewmodel


import androidx.lifecycle.*

import com.rockderick.drinksapp.domain.Repo
import com.rockderick.drinksapp.domain.model.Drink
import com.rockderick.drinksapp.domain.model.DrinkEntity
import com.rockderick.drinksapp.vo.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val repo: Repo): ViewModel() {

    private val tragosData = MutableLiveData<String>()

    fun setTrago(tragosName: String){
        tragosData.value = tragosName
    }

    init {
        setTrago("margarita")
    }

    val fetchTragosList = tragosData.distinctUntilChanged().switchMap {
        drinkName ->
        liveData(Dispatchers.IO){
            emit(Resource.Loading())
            try {
                emit(repo.getTragosList(drinkName))
            }catch (e: Exception){
                emit(Resource.Failure(e))
            }
        }
    }

    fun saveDrink(drink: DrinkEntity){
        viewModelScope.launch {
             repo.insertTrago(drink)
        }
    }

    fun getTragosFavoritos() = liveData(Dispatchers.IO){
        emit(Resource.Loading())
        try {
            emit(repo.getTragosFavoritos())
        }catch (e: Exception){
            emit(Resource.Failure(e))
        }
    }

    fun deleteDrink(drink: DrinkEntity){
        viewModelScope.launch {
            repo.deleteDrink(drink)
        }
    }

}