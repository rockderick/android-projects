package com.rockderick.test

import android.os.Handler
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.util.*
import kotlin.concurrent.schedule


class MainViewModel: ViewModel() {

    private val _text: MutableLiveData<String> = MutableLiveData()

    val text: LiveData<String> = _text


    init {
        // Use
        _text.value = "texto"

        val handler = Handler()
        handler.postDelayed({
            // do something after 1000ms
            _text.value = "cambia texto"

        }, 1000)
    }

}